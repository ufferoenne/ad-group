\section*{Task 2}
We will argue that the algorithm from task 1 works correctly.\\
\begin{lemma}
The $i$th iteration of the for loop of the algorithm has the loop invariant $b[j]=\bar{b}$ for all $j<i$.
\end{lemma}
\begin{proof}
Consider $i=1$ and the beginning of the first iteration of the for loop. Then the claim is that $b[j]=\bar{b}$ for all $j<1$, which is vacuously true.\\
Now assume that the claim $b[j]=\bar{b}$ for all $j<i$ is true at the beginning of the $i$th iteration, where $i<n-1$. We consider how $b[i]$ is altered. If $\text{surplus}=0$, then we do not alter $b[i]$, and so $b[i]=\bar{b}$. If $\text{surplus}>0$, then we put $b[i]=b[i]-(b[i]-\bar{b})=\bar{b}$. If $\text{surplus}<0$, we let $\text{deficit}=-\text{surplus}$ and put $b[i] = b[i] + \text{deficit} = b[i] - (b[i]-\bar{b})=\bar{b}$. Thus $b[i]=\bar{b}$ at the end of the $i$th iteration. Since the $i$th iteration does not change the value of $b[j]$ for any $j<i$, the loop invariant is maintained.\\
At the end of the $n-1$ iteration, we thus have that all of $b[1],b[2],\ldots,b[n-1]$ have the value $\bar{b}$. Therefore, at the termination of the loop, where $i=n$, the loop invariant is still maintained.
\end{proof}
As a consequence of this lemma, we can conclude that at the end of the algorithm, all values of $b[i]$ are $\bar{b}$, except possibly $b[n]$.\\
Since this is not an optimization problem, the greedy choice property and optimal substructure do not apply directly. However, we will make an argument that resembles these two properties, but first we will introduce a term to talk about the delivery of beers more formally.\\
Since a delivery of beer from one bar $j$ to another $i$ always goes through a bar neighboring bar $i$, the total movement of beer consists of a sequence of \emph{moves} $M$ between adjacent bars.\\
\begin{defn}
Let $b>0$, and let $i<n$. By a \emph{move} $M$, we mean either $M=M(i,i+1,b)$, which we define to be moving $b$ beers from bar $i$ to bar $i+1$, resulting in the new beer amounts
\begin{align*}
b_i' &= b_i - b,\\
b_{i+1}' &= b_{i+1} + \max(b-2\cdot(p_{i+1}-p_i),\ 0),
\end{align*}
or $M=M(i+1,i,b)$, which we define to move $b$ beers from bar $i+1$ to $i$, and thus
\begin{align*}
b_i' &= b_i + \max(b-2\cdot(p_{i+1}-p_i),\ 0),\\
b_{i+1}' &= b_{i+1} - b.
\end{align*}
\end{defn}
In our algorithm in task 1, we perform only one move for each $i<n$. Indeed, if $s=b[i]-\bar{b}>0$, then we perform the move $M(i,i+1,s)$, and if $s<0$, then we perform the move $M(i+1,i,-s+2(p_{i+1}-p_i))$. Note that in the definition of moves, we have no non-negativity restrictions on the resulting beer prices.\\
For our next result, we need to know that it does not matter in which order moves are performed.\\
\begin{lemma}
Moves commute.
\end{lemma}
\begin{proof}
Let $M$ and $M'$ be two moves. If they are moves on disjoint sets $\{i,i+1\}$ and $\{j,j+1\}$, then the two moves update disjoint array entries, and therefore the moves commute. Suppose instead that $M=M(i,i+1,b)$, and $M'=M(i+1,i+2,b')$. Then we need only check that the value of $b[i+1]$ is unaltered by changing the sequence of moves, but if we perform the moves in the order $M, M'$, then the value of $b[i+1]$ becomes
$$b[i+1] = \left(b[i+1] + \max(b-2\cdot(p_{i+1}-p_i),\ 0)\right) - b',$$
while if we perform the moves in the order $M',M$, then the value would instead become
$$b[i+1] = \left(b[i+1] - b'\right) +  \max(b-2\cdot(p_{i+1}-p_i),\ 0),$$
which is the same expression.\\
For other moves that work on the same entries, such as $M(i,i+1,b)$ and $M(i+1,i,b')$, or $M(i,i+1,b)$ and $M(i,i-1,b')$, you can do similar calculations that show that these moves also commute. This completes the proof.
\end{proof}
Our algorithm makes a greedy choice in that it chooses the first bar and evens out its beer storage. We will argue that this choice does not affect the decision problem.\\
\begin{lemma}
The answer to the size $n$ decision problem with beer amounts $(b_1,b_2,\ldots,b_n)$ is the same as the answer to the size $n-1$ decision problem with beer prices $(\widehat{b}_2,b_3,\ldots,b_n)$, where the latter beer amounts are obtained by completing the first iteration of our algorithm. We say that the greedy choice preserves the answer to the decision problem.
\end{lemma}
\begin{proof}
Assume first that the answer to the decision problem with the shortened beer array $(\widehat{b}_2,b_3,\ldots,b_n)$ is true. Then there exists a sequence $M_1, M_2, \ldots, M_L$ of moves which transforms $(\widehat{b}_2,b_3,\ldots,b_n)$ into an array of numbers all of which are at least $\bar{b}$. Since the original problem is transformed into this subproblem by the greedy choice using only one move $M_0$, and since this move leaves $\bar{b}$ beers at bar 1 (by the first lemma), we can place the move $M_0$ in front of $M_1, M_2, \ldots, M_L$ to get a sequence of moves proving that the answer to the decision problem for the original array $(b_1,b_2,\ldots,b_n)$ is true.\\
Assume then that the answer to the decision problem of size $n-1$ with beer amounts $(\widehat{b}_2,b_3,\ldots,b_n)$ is false. We want to prove that then the answer to the decision problem of size $n$ with beer amounts $(b_1, b_2, b_3,\ldots, b_n)$ is also false. This is more tricky, because even though we made one particular move from this problem of size $n$ to the problem of size $n-1$ (for which we know that the answer is false), we need a guarantee that no sequence of moves will transform the array $(b_1, b_2, b_3,\ldots, b_n)$ into one for which all entries are larger than $\bar{b}$. We proceed by contradiction, so assume we have a sequence of moves $M_1,M_2,\ldots,M_K$ which transforms the array $(b_1, b_2, b_3,\ldots, b_n)$ into one where all beer amounts are at least $\bar{b}$. Since moves commute by the second lemma, we can order them as we wish. Assume that there are exactly $J$ moves between bars $1$ and $2$, or vice versa, and let it be the first $J$ moves, i.e. our moves are now $M_1', M_2', \ldots, M_J', M_{J+1}', \ldots, M_K'$, where $M_i'$ has the form $M(1,2,b)$ or $M(2,1,b)$, if and only if $1\leq i\leq J$.\\
Assume that we have finished exactly the first $J$ moves. Then bar 1 must have at least $\bar{b}$ beers, since this bar is not touched by any subsequent moves. Also, the array will have the appearance $(b_1', b_2',b_3,\ldots,b_n)$, where $b_1'$ and $b_2'$ are the only values possibly altered compared to the original array. If the appearance of the beer amounts after the first iteration in our algorithm is $(\bar{b}, \widehat{b}_2,b_3,\ldots,b_n)$, then since $b_1'\geq \bar{b}$, we have $b_2'\leq \widehat{b}_2$. Also, since the moves $M_{J+1}',\ldots, M_K'$ transform $(b_2',b_3,\ldots,b_n)$ into a position, where all bars have beer amounts larger than $\bar{b}$, the same $K-J$ moves will do the same to the array $(\widehat{b}_2,b_3,\ldots,b_n)$, but this contradicts our original assumption that this array does not admit any such sequence of moves. This proves that if the answer to the decision problem of size $n-1$ with amounts $(\widehat{b}_2,b_3,\ldots,b_n)$ is false, then the answer to the problem of size $n$ with amounts $(b_1, b_2, b_3,\ldots, b_n)$ is also false.
\end{proof}
We can now state and prove the correctness of our algorithm.
\begin{thm}
Our algorithm from task 1 correctly solves the decision problem.
\end{thm}
\begin{proof}
We proceed by induction, so let $P(n)$ be the claim that our algorithm solves the decision problem correctly for $n$ bars. As base case, we can take $n=1$. Since there is only one bar, and our algorithm does nothing except return the boolean expression $b[1]\geq \bar{b}$, which is true if the bar has more than or equal to the required target, and otherwise false, then $P(1)$ is true.\\
Let $n>1$ be given, and assume that our algorithm solves the decision problem correctly for problems of size $n-1$. Since the algorithm is assumed to solve the decision problem correctly for problems of size $n-1$, and since the greedy choice preserves the answer to the decision problem by the third lemma, the algorithm correctly solves decision problems of size $n$. This completes the induction.
\end{proof}
% Prove that since b_1'\geq \bar{b} then b_2'\leq\tilde{b}?